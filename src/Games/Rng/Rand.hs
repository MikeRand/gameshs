module Games.Rng.Rand ( quantiles ) where

import System.Random ( mkStdGen, split, Random(randoms) )


quantiles :: (Random v) => Int -> [[v]]
quantiles initialSeed = recurQuantiles (mkStdGen initialSeed)
    where
        recurQuantiles g = randoms g : recurQuantiles (snd $ split g)