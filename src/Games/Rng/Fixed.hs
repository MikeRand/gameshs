module Games.Rng.Fixed ( quantiles) where

quantiles :: v -> [[v]]
quantiles value = repeat (repeat value)
