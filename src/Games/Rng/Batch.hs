module Games.Rng.Batch ( batches, horizon ) where

import Data.List ( unfoldr )


batches :: Int -> [[b]] -> [[[b]]]
batches = chunks
    where
        chunks n xs = takeWhile (not.null) $ unfoldr (Just . splitAt n) xs


horizon :: Int -> [[b]] -> [[b]]
horizon n = map (take n)
