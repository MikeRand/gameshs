module Games.Engine ( episode, episodeReturns ) where

import Data.List()


episode :: (s, [p], s -> Bool, s -> p, s -> [m], s -> m -> p -> v, s -> m -> s)
    -> (p -> (s -> v -> m))
    -> s
    -> [v]
    -> [(s, m, [(p, v)], s)]
episode game ppfs = recurEpisode
    where
        (_, players, isTerminal, currentPlayer, _, reward, nextState) = game
        recurEpisode _ [] = []
        recurEpisode s (q:qs)
            | isTerminal s = []
            | otherwise = (s, m, r, ns) : recurEpisode ns qs
                where
                    m = (ppfs $ currentPlayer s) s q
                    r = [(p, reward s m p) | p <- players]
                    ns = nextState s m


episodeReturns :: (RealFloat v) => v -> [(s, m, [(p, v)], s)] -> [(s, m, [(p, v)])]
episodeReturns _ [] = []
episodeReturns discount steps = zipWith (curry (\((s, m, _, _), r)->(s, m, r))) steps returns
    where
        rewards = map (\(_, _, r, _)->r) steps
        playerReturn rwd ret = rwd + discount * ret
        returns = scanr1 (combineByKey playerReturn) rewards


combineByKey :: (v -> v -> v) -> [(p, v)] -> [(p, v)] -> [(p, v)]
combineByKey f a b = [(p1, f v1 v2) | ((p1, v1), (_, v2)) <- zip a b]