module Games.Policy.GreedyQ
    ( ppf
    , pmf
    , finitePmf
    ) where

--import Debug.Trace (trace)

ppf :: (RealFloat v)
    => (s, [p], s -> Bool, s -> p, s -> [m], s -> m -> p -> v, s -> m -> s)
    -> (v -> v -> Bool)
    -> ((s, m, p) -> v)
    -> s -> v -> m
ppf game eq moveValue state quantile = getMove idx moveList
  where
    --(_, _, _, _, moves, _, _) = game
    moveList = maxMoves game eq moveValue state
    idx = floor (quantile * fromIntegral (length moveList))
    --getMove i l | trace ("|moves|=" ++ show (length (moves state)) ++ ", index=" ++ show i ++ ", |moveList|=" ++ show (length l)) False = undefined
    getMove i l = l !! i


pmf :: (RealFloat v, Eq m)
    => (s, [p], s -> Bool, s -> p, s -> [m], s -> m -> p -> v, s -> m -> s)
    -> (v -> v -> Bool)
    -> ((s, m, p) -> v)
    -> s -> m -> v
pmf game eq moveValue state move
    | move `elem` moveList = p moveList
    | otherwise = 0
  where
    moveList = maxMoves game eq moveValue state
    p l = 1.0 / fromIntegral (length l)


finitePmf :: (RealFloat v)
    => (s, [p], s -> Bool, s -> p, s -> [m], s -> m -> p -> v, s -> m -> s)
    -> (v -> v -> Bool)
    -> ((s, m, p) -> v)
    -> s
    -> [(m, v)]
finitePmf game eq moveValue state = map (\x -> (x, p)) moveList
  where
    moveList = maxMoves game eq moveValue state
    p = 1.0 / fromIntegral (length moveList)


maxMoves :: (RealFloat v)
    => (s, [p], s -> Bool, s -> p, s -> [m], s -> m -> p -> v, s -> m -> s)
    -> (v -> v -> Bool)
    -> ((s, m, p) -> v)
    -> s -> [m]
maxMoves game eq moveValue state = maximumsBy (\m->moveValue (state, m, cp)) eq (moves state)
  where
    (_, _, _, currentPlayer, moves, _, _) = game
    cp = currentPlayer state


maximumsBy :: (Ord b) => (a -> b) -> (b -> b -> Bool) -> [a] -> [a]
maximumsBy _ _ [] = []
maximumsBy f eq xs = filter (eq maxval . f) xs
  where
    maxval = maximum $ map f xs


