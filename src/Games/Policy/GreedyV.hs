module Games.Policy.GreedyV
    ( ppf
    , pmf
    , finitePmf
    ) where


ppf :: (RealFloat v)
    => (s, [p], s -> Bool, s -> p, s -> [m], s -> m -> p -> v, s -> m -> s)
    -> (v -> v -> Bool)
    -> ((s, p) -> v)
    -> s -> v -> m
ppf game eq stateValue state quantile = moveList !! idx
  where
    moveList = maxMoves game eq stateValue state
    idx = floor (quantile * fromIntegral (length moveList))


pmf :: (RealFloat v, Eq m)
    => (s, [p], s -> Bool, s -> p, s -> [m], s -> m -> p -> v, s -> m -> s)
    -> (v -> v -> Bool)
    -> ((s, p) -> v)
    -> s -> m -> v
pmf game eq stateValue state move
    | move `elem` moveList = p
    | otherwise = 0
  where
    moveList = maxMoves game eq stateValue state
    p = 1.0 / fromIntegral (length moveList)


finitePmf :: (RealFloat v)
    => (s, [p], s -> Bool, s -> p, s -> [m], s -> m -> p -> v, s -> m -> s)
    -> (v -> v -> Bool)
    -> ((s, p) -> v)
    -> s
    -> [(m, v)]
finitePmf game eq stateValue state = map (\x -> (x, p)) moveList
  where
    moveList = maxMoves game eq stateValue state
    p = 1.0 / fromIntegral (length moveList)


maxMoves :: (RealFloat v)
    => (s, [p], s -> Bool, s -> p, s -> [m], s -> m -> p -> v, s -> m -> s)
    -> (v -> v -> Bool)
    -> ((s, p) -> v)
    -> s -> [m]
maxMoves game eq stateValue state = maximumsBy moveValue eq (moves state)
  where
    (_, _, _, currentPlayer, moves, reward, nextState) = game
    cp = currentPlayer state
    moveValue m = rv + nsv
      where
        rv = reward state m cp
        ns = nextState state m
        nsv = stateValue (ns, cp)


maximumsBy :: (Ord b) => (a -> b) -> (b -> b -> Bool) -> [a] -> [a]
maximumsBy _ _ [] = []
maximumsBy f eq xs = filter (eq maxval . f) xs
  where
    maxval = maximum $ map f xs


