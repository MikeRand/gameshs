module Games.Policy.Soft
    ( ppf
    , pmf
    , finitePmf
    ) where


ppf :: (RealFloat v)
    => (s, [p], s -> Bool, s -> p, s -> [m], s -> m -> p -> v, s -> m -> s)
    -> v
    -> (s -> v -> m)
    -> s -> v -> m
ppf game epsilon ppfBase state quantile
    | quantile < epsilon = moveList !! idx
    | otherwise = ppfBase state ((quantile - epsilon) / (1 - epsilon))
        where
          (_, _, _, _, moves, _, _) = game
          moveList = moves state
          p = quantile / epsilon
          idx = floor (p * fromIntegral (length moveList))

pmf :: (RealFloat v)
    => (s, [p], s -> Bool, s -> p, s -> [m], s -> m -> p -> v, s -> m -> s)
    -> v
    -> (s -> m -> v)
    -> s -> m -> v
pmf game epsilon pmfBase state move = epsilon * p + (1 - epsilon) * pmfBase state move
  where
    (_, _, _, _, moves, _, _) = game
    moveList = moves state
    p = 1.0 / fromIntegral (length moveList)


finitePmf :: (RealFloat v)
    => (s, [p], s -> Bool, s -> p, s -> [m], s -> m -> p -> v, s -> m -> s)
    -> v
    -> (s -> m -> v)
    -> s -> [(m, v)]
finitePmf game epsilon pmfBase state = [(m, epsilon * p + (1 - epsilon) * pmfBase state m) | m <- moveList]
  where
    (_, _, _, _, moves, _, _) = game
    moveList = moves state
    p = 1.0 / fromIntegral (length moveList)
