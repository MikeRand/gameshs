{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DerivingStrategies#-}

module Games.IO.Draft
    ( decodeJSON
    , DraftGame
    , owners
    , rounds
    , isSnake
    , players
    , positionToRosterSpots
    , startingRosterSpots
    ) where


import Control.Applicative (empty)
import qualified Data.ByteString.Lazy.Char8 as BL
import Data.Aeson
    ( pairs,
      (.:),
      object,
      FromJSON(parseJSON),
      Value(Object),
      KeyValue((.=)),
      ToJSON(toJSON, toEncoding),
      decode)


data DraftGame = DraftGame
    { owners :: [String]
    , rounds :: Int
    , isSnake :: Bool
    , players :: [(String, String, [String], [Double])]
    , positionToRosterSpots :: [(String, [String])]
    , startingRosterSpots :: [(String, Int)]
    } deriving stock (Show)


instance ToJSON DraftGame where
    toJSON (DraftGame ownersV roundsV isSnakeV playersV positionToRosterSpotsV startingRosterSpotsV) = object [ "owners" .= ownersV
                                                                                                              , "rounds" .= roundsV
                                                                                                              , "isSnake" .= isSnakeV
                                                                                                              , "players" .= playersV
                                                                                                              , "positionToRosterSpots" .= positionToRosterSpotsV
                                                                                                              , "startingRosterSpots" .= startingRosterSpotsV
                                                                                                              ]
    toEncoding DraftGame{..} = pairs $
        "owners" .= owners <>
        "rounds" .= rounds <>
        "isSnake" .= isSnake <>
        "players" .= players <>
        "positionToRosterSpots" .= positionToRosterSpots <>
        "startingRosterSpots" .= startingRosterSpots


instance FromJSON DraftGame where
    parseJSON (Object v) = DraftGame <$>
                           v .: "owners" <*>
                           v .: "rounds" <*>
                           v .: "isSnake" <*>
                           v .: "players" <*>
                           v .: "positionToRosterSpots" <*>
                           v .: "startingRosterSpots"
    parseJSON _          = empty


decodeJSON :: String -> Maybe DraftGame
decodeJSON j = decode $ BL.pack j