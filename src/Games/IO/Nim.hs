{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DerivingStrategies#-}

module Games.IO.Nim 
    ( decodeJSON
    , NimGame
    , players
    , initialPiles
    , isMisere
    ) where


import Control.Applicative (empty)
import qualified Data.ByteString.Lazy.Char8 as BL
import Data.Aeson
    ( pairs,
      (.:),
      object,
      FromJSON(parseJSON),
      Value(Object),
      KeyValue((.=)),
      ToJSON(toJSON, toEncoding),
      decode)


data NimGame = NimGame
    { players :: [String]
    , initialPiles :: [Int]
    , isMisere :: Bool
    } deriving stock (Show)


instance ToJSON NimGame where
    toJSON (NimGame playersV initialPilesV isMisereV) = object [ "players" .= playersV,
                                                                 "initialPiles" .= initialPilesV,
                                                                 "isMisere" .= isMisereV]
    toEncoding NimGame{..} = pairs $
        "players" .= players <>
        "initialPiles" .= initialPiles <>
        "isMisere" .= isMisere


instance FromJSON NimGame where
    parseJSON (Object v) = NimGame <$>
                           v .: "players" <*>
                           v .: "initialPiles" <*>
                           v .: "isMisere"
    parseJSON _          = empty


decodeJSON :: String -> Maybe NimGame
decodeJSON j = decode $ BL.pack j