module Games.Learn.ValueIteration ( learnPolicy ) where

import Data.List ( foldl' )
import Data.Set ( singleton, insert, member )
import Data.Graph ( graphFromEdges, topSort )
import Data.Hashable ( Hashable )

import Games.Policy.GreedyV as GreedyV ( finitePmf, ppf)

import Games.Value.Tabular as TabularValue
    ( ValueTable
    , tabularValue
    , newValueTable
    , updateValueTable
    )


learnPolicy :: (Hashable s, Ord s, Eq p, Hashable p, Eq m, RealFloat v)
    => (s, [p], s -> Bool, s -> p, s -> [m], s -> m -> p -> v, s -> m -> s)
    -> v
    -> (v -> v -> Bool)
    -> s
    -> [(s -> v -> m, s -> [(m, v)])]
learnPolicy game defv eq state = map policy (recurLearn newValueTable)
  where
    (_, _, isTerminal, _, moves, _, nextState) = game
    states = statesTopSort moves nextState state
    stateValueFromTable = TabularValue.tabularValue (isTerminal . fst) defv
    ppfFromTable t = GreedyV.ppf game eq (stateValueFromTable t)
    pmfFromTable t = GreedyV.finitePmf game eq (stateValueFromTable t)
    policy table = (ppfFromTable table, pmfFromTable table)
    recurLearn table
        | policyIsStable states (pmfFromTable newTable) (pmfFromTable table) = [newTable]
        | otherwise = newTable : recurLearn newTable
      where
        newTable = foldl' (updateStateValue game defv stateValueFromTable pmfFromTable) table states


policyIsStable :: (Eq m, RealFloat v)
    => [s]
    -> (s -> [(m, v)])
    -> (s -> [(m, v)])
    -> Bool
policyIsStable states new old = all stateIsStable states
  where
    stateIsStable s = new s == old s


updateStateValue :: (Eq s, Hashable s, Eq p, Hashable p, RealFloat v)
    => (s, [p], s -> Bool, s -> p, s -> [m], s -> m -> p -> v, s -> m -> s)
    -> v
    -> (ValueTable (s, p) v -> (s, p) -> v)
    -> (ValueTable (s, p) v -> s -> [(m, v)])
    -> ValueTable (s, p) v
    -> s
    -> ValueTable (s, p) v
updateStateValue game defv stateValueFromTable pmfFromTable table state = foldl' updatePlayer table players
  where
    (_, players, _, _, _, reward, nextState) = game
    stateValue = stateValueFromTable table
    policy = pmfFromTable table state
    decay = 1 -- full overwrite
    updatePlayer t p = updateValueTable defv 0 decay t ((state, p), v, w)
      where
        moveValue m = r + nsv
          where 
            r = reward state m p
            ns = nextState state m
            nsv = stateValue (ns, p)
        v = expectation [(moveValue m, prob) | (m, prob) <- policy]
        w = 1


expectation :: (Num a) => [(a, a)] -> a
expectation [] = 0
expectation xs = sum $ map (uncurry (*)) xs


statesTopSort :: (Ord s) => (s -> [a]) -> (s -> a -> s) -> s -> [s]
statesTopSort actions nextState state = reverse (map (\(a, _, _) -> a) states)
  where
    edgeList = gameEdges actions nextState state
    (graph, nodeFromVertex, _) = graphFromEdges edgeList
    states = map nodeFromVertex (topSort graph)


gameEdges :: (Ord s) => (s -> [a]) -> (s -> a -> s) -> s -> [(s, s, [s])]
gameEdges actions nextState state = recurGameEdges state edges [] edges (singleton state)
  where
    stateEdges s = map (nextState s) (actions s)
    edges = stateEdges state
    recurGameEdges s e sToProcess eToProcess visited
        | null sToProcess && null eToProcess = [(s, s, e)]
        | null eToProcess = (s, s, e) : recurGameEdges (head sToProcess) (stateEdges (head sToProcess)) (tail sToProcess) (stateEdges (head sToProcess)) visited
        | member (head eToProcess) visited = recurGameEdges s e sToProcess (tail eToProcess) visited
        | otherwise = recurGameEdges s e (head eToProcess : sToProcess) (tail eToProcess) (insert (head eToProcess) visited)
