module Games.Learn.MonteCarloOnPolicy ( learnPolicy ) where

import Data.List ( foldl' )
import Data.Hashable ( Hashable )

import Games.Engine as Engine ( episode, episodeReturns)
import Games.Policy.GreedyV as GreedyV ( finitePmf, ppf)
import Games.Policy.Soft as Soft ( ppf)
import Games.Value.Tabular as TabularValue
    ( ValueTable
    , tabularValue
    , newValueTable
    , updateValueTable
    )


learnPolicy :: (Hashable s, Ord s, Eq p, Hashable p, RealFloat v)
    => (s, [p], s -> Bool, s -> p, s -> [m], s -> m -> p -> v, s -> m -> s)
    -> v
    -> (v -> v -> Bool)
    -> v
    -> [[[v]]]
    -> s
    -> [(s -> v -> m, s -> [(m, v)])]
learnPolicy game defv eq eps qs state = map policy (recurLearn TabularValue.newValueTable qs)
  where
    (_, _, isTerminal, _, _, _, _) = game
    --batch update
    stateValueFromTable = TabularValue.tabularValue (isTerminal . fst) defv  -- careful. fst assumes tuples, not triples+
    greedyPpfFromTable t = GreedyV.ppf game eq (stateValueFromTable t)
    softPpfFromTable t = Soft.ppf game eps (greedyPpfFromTable t)
    episodeReturnsFromPolicy pol = episodeReturns 1.0 . episode game (const pol) state
    decay = 0 -- weighted avg.
    update = updatePlayer defv decay
    runBatch = batch softPpfFromTable episodeReturnsFromPolicy update
    --completion
    greedyPmfFromTable t = GreedyV.finitePmf game eq (stateValueFromTable t)
    policy t = (greedyPpfFromTable t, greedyPmfFromTable t)
    --learn
    recurLearn _ [] = []
    recurLearn t (x:xs) = t : recurLearn (runBatch t x) xs


updatePlayer :: (Eq s, Hashable s, Eq p, Hashable p, RealFloat v)
    => v
    -> v
    -> s
    -> ValueTable (s, p) v
    -> (p, v)
    -> ValueTable (s, p) v
updatePlayer defv decay s t (p, v) = updateValueTable defv 0 decay t ((s, p), v, 1)


updateStep :: (s -> ValueTable (s, p) v -> (p, v) -> ValueTable (s, p) v)
    -> ValueTable (s, p) v
    -> (s, m, [(p, v)])
    -> ValueTable (s, p) v
updateStep update t (s, _, r) = foldl' (update s) t r


updateEpisode :: (s -> ValueTable (s, p) v -> (p, v) -> ValueTable (s, p) v)
    -> ValueTable (s, p) v
    -> [(s, m, [(p, v)])]
    -> ValueTable (s, p) v
updateEpisode update = foldl' (updateStep update)


updateBatch :: (s -> ValueTable (s, p) v -> (p, v) -> ValueTable (s, p) v)
    -> ValueTable (s, p) v
    -> [[(s, m, [(p, v)])]]
    -> ValueTable (s, p) v
updateBatch update = foldl' (updateEpisode update)


batch :: (ValueTable (s, p) v -> (s -> v -> m))
    -> ((s -> v -> m) -> [v] -> [(s, m, [(p, v)])])
    -> (s -> ValueTable (s, p) v -> (p, v) -> ValueTable (s, p) v)
    -> ValueTable (s, p) v
    -> [[v]]
    -> ValueTable (s, p) v
batch ppfFromTable returnsFromPolicy update table rs = updateBatch update table eReturns
    where
        pol = ppfFromTable table
        returnsFromQuantiles = returnsFromPolicy pol
        eReturns = map returnsFromQuantiles rs
