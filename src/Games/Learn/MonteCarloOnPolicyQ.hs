module Games.Learn.MonteCarloOnPolicyQ ( learnPolicy ) where

import Data.List ( foldl' )
import Data.Hashable ( Hashable )

import Games.Engine as Engine ( episode, episodeReturns)
import Games.Policy.GreedyQ as GreedyQ ( finitePmf, ppf, pmf)
import Games.Policy.Soft as Soft ( ppf, pmf)
import Games.Value.Tabular as TabularValue
    ( ValueTable
    , tabularValue
    , newValueTable
    , updateValueTable
    )


learnPolicy :: (Hashable s, Ord s, Eq p, Hashable p, Eq m, Hashable m, RealFloat v)
    => (s, [p], s -> Bool, s -> p, s -> [m], s -> m -> p -> v, s -> m -> s)
    -> v
    -> (v -> v -> Bool)
    -> v
    -> [[[v]]]
    -> s
    -> [(s -> v -> m, s -> [(m, v)])]
learnPolicy game defv eq eps qs state = map policy (recurLearn TabularValue.newValueTable qs)
  where
    (_, _, isTerminal, _, _, _, _) = game
    --batch update
    moveValueFromTable = TabularValue.tabularValue (isTerminal . \(s,_,_)->s) defv
    greedyPpfFromTable t = GreedyQ.ppf game eq (moveValueFromTable t)
    greedyPmfFromTable t = GreedyQ.pmf game eq (moveValueFromTable t)
    softPpfFromTable t = Soft.ppf game eps (greedyPpfFromTable t)
    softPmfFromTable t = Soft.pmf game eps (greedyPmfFromTable t)
    episodeReturnsFromPolicy pol = episodeReturns 1.0 . episode game (const pol) state
    decay = 0 -- weighted avg.
    update = updatePlayer defv decay
    runBatch = batch softPpfFromTable softPmfFromTable softPmfFromTable episodeReturnsFromPolicy update
    --completion
    greedyFinitePmfFromTable t = GreedyQ.finitePmf game eq (moveValueFromTable t)
    policy t = (greedyPpfFromTable t, greedyFinitePmfFromTable t)
    --learn
    recurLearn _ [] = []
    recurLearn t (x:xs) = t : recurLearn (runBatch t x) xs


updatePlayer :: (Eq s, Hashable s, Eq m, Hashable m, Eq p, Hashable p, RealFloat v)
    => v -> v -> s -> m -> v
    -> ValueTable (s, m, p) v
    -> (p, v)
    -> ValueTable (s, m, p) v
updatePlayer defv decay s m w t (p, v) = updateValueTable defv 0 decay t ((s, m, p), v, w)


updateStep :: (RealFloat v)
    => (s -> m -> v)
    -> (ValueTable (s, m, p) v -> (s -> m -> v))
    -> (s -> m -> v -> ValueTable (s, m, p) v -> (p, v) -> ValueTable (s, m, p) v)
    -> (s, m, [(p, v)])
    -> (ValueTable (s, m, p) v, v)
    -> (ValueTable (s, m, p) v, v)
updateStep behaviorPmf targetPmfFromTable update (s, m, r) (t, w) = (newTable, newW)
    where
        newTable = foldl' (update s m w) t r
        bWeight = behaviorPmf s m
        targetPmf = targetPmfFromTable newTable
        tWeight = targetPmf s m
        newW = w * tWeight / bWeight


updateEpisode :: (RealFloat v)
    => (ValueTable (s, m, p) v -> (s -> v -> m))
    -> (ValueTable (s, m, p) v -> (s -> m -> v))
    -> (ValueTable (s, m, p) v -> (s -> m -> v))
    -> ((s -> v -> m) -> [v] -> [(s, m, [(p, v)])])
    -> (s -> m -> v -> ValueTable (s, m, p) v -> (p, v) -> ValueTable (s, m, p) v)
    -> ValueTable (s, m, p) v
    -> [v]
    -> ValueTable (s, m, p) v
updateEpisode behaviorPpfFromTable behaviorPmfFromTable targetPmfFromTable returnsFromPolicy update t rs = fst (foldr (updateStep behaviorPmf targetPmfFromTable update) (t, 1) (epReturns rs))
    where
        behaviorPmf = behaviorPmfFromTable t
        behaviorPpf = behaviorPpfFromTable t
        epReturns q = returnsFromPolicy behaviorPpf q


batch :: (RealFloat v)
    => (ValueTable (s, m, p) v -> (s -> v -> m))
    -> (ValueTable (s, m, p) v -> (s -> m -> v))
    -> (ValueTable (s, m, p) v -> (s -> m -> v))
    -> ((s -> v -> m) -> [v] -> [(s, m, [(p, v)])])
    -> (s -> m -> v -> ValueTable (s, m, p) v -> (p, v) -> ValueTable (s, m, p) v)
    -> ValueTable (s, m, p) v
    -> [[v]]
    -> ValueTable (s, m, p) v
batch behaviorPpfFromTable behaviorPmfFromTable targetPmfFromTable returnsFromPolicy update = foldl' (updateEpisode behaviorPpfFromTable behaviorPmfFromTable targetPmfFromTable returnsFromPolicy update)
        
