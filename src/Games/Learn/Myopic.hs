module Games.Learn.Myopic ( learnPolicy ) where

import Games.Policy.GreedyQ ( ppf, finitePmf )


learnPolicy :: (RealFloat v)
    => (s, [p], s -> Bool, s -> p, s -> [m], s -> m -> p -> v, s -> m -> s)
    -> (v -> v -> Bool)
    -> [(s -> v -> m, s -> [(m, v)])]
learnPolicy game eq = [ (ppf game eq mv, finitePmf game eq mv) ]
  where
    (_, _, _, _, _, reward, _) = game
    mv (s, m, p) = reward s m p 
