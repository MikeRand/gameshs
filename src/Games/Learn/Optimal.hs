module Games.Learn.Optimal ( learnPolicy ) where

import Games.Value.TreeSearch ( stateValue )
import Games.Policy.GreedyV ( ppf, finitePmf )


learnPolicy :: (RealFloat v)
    => (s, [p], s -> Bool, s -> p, s -> [m], s -> m -> p -> v, s -> m -> s)
    -> (v -> v -> Bool)
    -> [(s -> v -> m, s -> [(m, v)])]
learnPolicy game eq = [ (ppf game eq sv, finitePmf game eq sv) ]
    where sv = stateValue game eq
