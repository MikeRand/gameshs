module Games.Value.TreeSearch ( stateValue ) where

import Games.Policy.GreedyV ( finitePmf )


stateValue :: (RealFloat v)
    => (s, [p], s -> Bool, s -> p, s -> [m], s -> m -> p -> v, s -> m -> s)
    -> (v -> v -> Bool)
    -> ((s, p) -> v)
stateValue game eq = recurSV
  where
     (_, _, isTerminal, _, _, reward, nextState) = game
     fPmf = finitePmf game eq recurSV
     recurSV (s, p)
        | isTerminal s = 0
        | otherwise = expectation moveValue (fPmf s)
            where
              moveValue m = r + vns
                where
                  r = reward s m p
                  ns = nextState s m
                  vns = recurSV (ns, p)


expectation :: (RealFloat v) => (m -> v) -> [(m, v)] -> v
expectation moveValue pmf = sum (map (\(move, prob)->prob * moveValue move) pmf)



