module Games.Value.Tabular
    ( tabularValue
    , tabularVar
    , updateValueTable
    , newValueTable
    , ValueTable
    ) where

import Data.HashMap.Strict ( HashMap, empty, findWithDefault, insert )
import Data.Hashable ( Hashable )


data ValueTable k v = ValueTable
    { means :: HashMap k v
    , weights :: HashMap k v
    , var :: HashMap k v
    }


tabularValue :: (Eq k, Hashable k, RealFloat v)
    => (k -> Bool) -> v -> ValueTable k v -> k -> v
tabularValue isTerminal defv t k
    | isTerminal k = 0
    | otherwise = findWithDefault defv k (means t)


tabularVar :: (Eq k, Hashable k, RealFloat v)
    => (k -> Bool) -> v -> ValueTable k v -> k -> v
tabularVar isTerminal defvar t k
    | isTerminal k = 0
    | weight == 0 = defvar
    | otherwise = findWithDefault 0 k (var t) / weight
        where
            weight = findWithDefault 0 k (weights t)

updateValueTable :: (Eq k, Hashable k, RealFloat v)
    => v -> v -> v -> ValueTable k v -> (k, v, v) -> ValueTable k v
updateValueTable defv defw decay t (k, v, w)
    | w == 0.0 = t
    | otherwise = ValueTable {means=mMap, weights=wMap, var=varMap}
        where
            ow = findWithDefault defw k (weights t)
            nw = (1 - decay) * ow + w
            wMap = insert k nw (weights t)
            om = findWithDefault defv k (means t)
            nm = ((1 - decay) * ow * om + v * w) / nw
            mMap = insert k nm (means t)
            ovar = findWithDefault 0 k (var t)
            nvar = ((1 - decay) * ow * ovar + w * (v - nm) * (v - om)) / nw
            varMap = insert k nvar (var t)

newValueTable :: ValueTable k v
newValueTable = ValueTable empty empty empty
