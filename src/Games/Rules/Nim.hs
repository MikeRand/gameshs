{-# LANGUAGE DerivingStrategies#-}

module Games.Rules.Nim ( newGame ) where


import Data.Hashable ( Hashable ( hashWithSalt ) )


newGame :: (Eq p, RealFloat v)
  => [p] -> [Int] -> Bool
  -> (State, [p], State -> Bool, State -> p, State -> [[Int]], State -> [Int] -> p -> v, State -> [Int] -> State)
newGame players initialPiles isMisere = (iS, p, iT, cP, m, r, ns)
  where
    iS = initialState initialPiles
    p = players
    iT = isTerminal
    cP = currentPlayer players
    m = moves
    r = reward players isMisere
    ns = nextState players


data State = State
  { playerIdx :: Int
  , piles :: [Int]
  } deriving stock (Eq, Ord, Show) 

instance Hashable State where
  hashWithSalt s (State pIdx p) =
    s `hashWithSalt`
    pIdx `hashWithSalt` p


initialState :: [Int] -> State
initialState initialPiles = State{playerIdx=0, piles=initialPiles}

isTerminal :: State -> Bool
isTerminal state = sum (piles state) == 0

currentPlayer :: [p] -> State -> p
currentPlayer players state = players !! playerIdx state

moves :: State -> [[Int]]
moves state
    | isTerminal state = []
    | otherwise = recurMoves (piles state) 0 1
  where
    recurMoves pls pile action
        | pile == length pls = []
        | action > pls !! pile = recurMoves pls (pile + 1) 1
        | action == pls !! pile = makeMove action pile : recurMoves pls (pile + 1) 1
        | otherwise = makeMove action pile : recurMoves pls pile (action + 1)
      where
        makeMove m p = [if i==p then m else 0 | i<-[0..n-1]]
          where
            n = length pls

reward :: (Eq p, RealFloat v) => [p] -> Bool -> State -> [Int] -> p -> v
reward players isMisere state action player
    | piles state /= action = 0
    | isMisere && isCurrentPlayer = fromIntegral $ - (length players - 1)
    | isMisere = 1
    | isCurrentPlayer = fromIntegral $ length players - 1
    | otherwise = - 1
  where
    isCurrentPlayer = currentPlayer players state == player

nextState :: [p] -> State -> [Int] -> State
nextState players state action = State{playerIdx=nextPlayerIdx, piles=nextPiles}
  where
    nextPlayerIdx = (playerIdx state + 1) `rem` length players
    nextPiles = zipWith (-) (piles state) action
  