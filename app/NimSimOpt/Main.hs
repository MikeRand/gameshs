module Main where

import qualified Data.List as List
import qualified System.Environment as Environment
import qualified System.Exit as Exit

import qualified Games.Engine as Engine
import qualified Games.IO.Nim as NimIO
import qualified Games.Rules.Nim as Nim
import qualified Games.Learn.Optimal as Opt
import qualified Games.Rng.Rand as RNG

processArgs :: IO ()
processArgs = do
    args <- Environment.getArgs
    case args of
        [fname, ta, na] -> do
            let t = read ta :: Int
            let n = read na :: Int
            case (fname, t, n) of
                (_, 0, 0) -> exitWithError "N & T must be > 0"
                (_, 0, _) -> exitWithError "T must be > 0"
                (_, _, 0) -> exitWithError "N must be > 0"
                _ -> do processParams fname t n
        _ -> exitWithError "Need to provide T > 0, N > 0"


processParams :: String -> Int -> Int -> IO ()
processParams fname t n = do
    params <- readFile fname
    let result = NimIO.decodeJSON params :: Maybe NimIO.NimGame
    case result of
        Nothing -> exitWithError "Error reading parameter file."
        Just game -> do runSimulation t n game


runSimulation :: Int -> Int -> NimIO.NimGame -> IO ()
runSimulation t n gameParams = do
    putStr "Let's play some Nim: "
    let players = NimIO.players gameParams
    putStr "players="
    putStr $ show players
    let initialPiles = NimIO.initialPiles gameParams
    putStr ", initialPiles="
    putStr $ show initialPiles
    let isMisere = NimIO.isMisere gameParams
    putStr ", isMisere="
    print isMisere
    let game = Nim.newGame players initialPiles isMisere
    let (iS, _, _, _, _, _, _) = game
    -- learn a policy
    putStr "Taking up to "
    putStr $ show t
    putStrLn " policies."
    let ve = (\x y->abs (x - y) < 1e-6)
    let policies = take t (Opt.learnPolicy game ve)
    putStr "Took "
    putStr $ show (length policies)
    putStrLn " policies."
    let (ppf, _) = last policies
    let ppfs = const ppf
    -- play the game n times and tabulate the results.
    putStr "Playing game "
    putStr $ show n
    putStrLn " times."
    let s = iS
    let e = Engine.episode game ppfs s
    let rs = take n (RNG.quantiles 42::[[Double]])
    -- here is the opportunity to parMap
    let results = map ((\(_, _, r)->r) . head . Engine.episodeReturns 1 . e) rs
    let totalScore = List.foldl1' (combineByKey (+)) results
    putStr "Final score: "
    print totalScore


combineByKey :: (v -> v -> v) -> [(p, v)] -> [(p, v)] -> [(p, v)]
combineByKey f a b = [(p1, f v1 v2) | ((p1, v1), (_, v2)) <- zip a b]


exitWithError :: String -> IO a
exitWithError err = do putStrLn err
                       Exit.exitWith (Exit.ExitFailure 1)


main :: IO ()
main = processArgs
