module Games.Rng.RandSpec ( spec ) where

import Test.Hspec ( shouldBe, it, describe, Spec )
import Games.Rng.Rand as RNG ( quantiles )


spec :: Spec
spec = do
  describe "Random Quantiles" $ do
    let initialSeed = 0 :: Int
    let rs = RNG.quantiles initialSeed :: [[Double]]

    it "returns proper batches" $ do
      let batch = take 10 rs
      let episode = head batch
      take 5 episode `shouldBe` [0.9871468153391151,6.761085639865827e-2,5.591622274642816e-2,0.6213899146323314,0.7670053614498886]

    it "returns different rs by episodes" $ do
      let batch = take 2 rs
      let sndEpisode = batch !! 1
      take 5 sndEpisode `shouldBe` [0.38267073693838793,0.9115278142182447,0.7608539047579249,0.5974826840143014,0.34203513730713286]

