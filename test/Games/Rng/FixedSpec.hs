module Games.Rng.FixedSpec ( spec ) where

import Test.Hspec ( shouldBe, it, describe, Spec )
import Games.Rng.Fixed as RNG ( quantiles )


spec :: Spec
spec = do
  describe "Constant Quantiles" $ do
    let v = 0.7 :: Double
    let rs = RNG.quantiles v::[[Double]]

    it "returns constant episodes" $ do
      let batch = take 10 rs
      let fstEpisode = head batch
      let sndEpisode = batch !! 1
      take 5 fstEpisode `shouldBe` take 5 sndEpisode
