module Games.Rng.BatchSpec ( spec ) where

import Test.Hspec ( shouldBe, it, describe, Spec )
import Games.Rng.Rand as RNG ( quantiles )
import Games.Rng.Batch as Batch ( batches, horizon )


spec :: Spec
spec = do
  describe "Random batches" $ do
    let batchSize = 10
    let rs = batches batchSize (RNG.quantiles 42::[[Double]])

    it "returns batches of the correct length" $ do
      all (\batch-> length batch==batchSize) (take 5 rs) `shouldBe` True

    it "returns batches with different episodes" $ do
      let batch = head rs
      let fstEpisode = take 5 (head batch)
      let sndEpisode = take 5 (batch !! 1)
      (fstEpisode, sndEpisode) `shouldBe` ([1.0663729393723398e-2,0.9827538369038856,0.7042944187434987,0.11924823950991781,0.2625506456047775],[0.7740913257381021,0.28092505248377475,0.41457515913407805,0.4242882501294888,0.2754983382152698])


    it "returns different episodes across batches" $ do
      let fstEpisode = take 5 $ head $ head (take 2 rs)
      let sndEpisode = take 5 $ head (take 2 rs) !! 1
      (fstEpisode, sndEpisode) `shouldBe` ([1.0663729393723398e-2,0.9827538369038856,0.7042944187434987,0.11924823950991781,0.2625506456047775],[0.7740913257381021,0.28092505248377475,0.41457515913407805,0.4242882501294888,0.2754983382152698])

  describe "Random horizons" $ do
    let horizonSize = 10
    let rs = horizon horizonSize (RNG.quantiles 42::[[Double]])

    it "returns episodes of the correct length" $ do
      all (\episode-> length episode==horizonSize) (take 5 rs) `shouldBe` True

    it "returns different episodes" $ do
      let fstEpisode = take 5 $ head (take 2 rs)
      let sndEpisode = take 5 $ take 2 rs !! 1
      (fstEpisode, sndEpisode) `shouldBe` ([1.0663729393723398e-2,0.9827538369038856,0.7042944187434987,0.11924823950991781,0.2625506456047775],[0.7740913257381021,0.28092505248377475,0.41457515913407805,0.4242882501294888,0.2754983382152698])
