module Games.Value.TabularSpec ( spec ) where

import Test.Hspec ( shouldBe, it, describe, Spec )
import Games.Value.Tabular as TabularValue ( newValueTable, updateValueTable, tabularValue, tabularVar )


floatEq :: (RealFloat v) => v -> v -> v -> Bool
floatEq tol a b = abs (a - b) < tol


spec :: Spec
spec = do
  let fEq = floatEq 1e-6 -- Float has 6 digits of precision
  let infinity = read "Infinity"::Float
  describe "Empty list" $ do
    let defv = 2.0 :: Float
    let t = newValueTable
    it "returns default value when isTerminal=False" $ do
      tabularValue (const False) defv t "Key" `shouldBe` defv

    it "returns terminal value when isTerminal=True" $ do
      tabularValue (const True) defv t "Key" `shouldBe` 0.0

    it "returns default var when isTerminal=False" $ do
      tabularVar (const False) infinity t "Key" `shouldBe` infinity

    it "returns terminal var when isTerminal=False" $ do
      tabularVar (const True) infinity t "Key" `shouldBe` 0.0

  describe "Update into empty list with no default weight" $ do
    let defv = 2.0 :: Float
    let defw = 0.0 :: Float
    let t = newValueTable
    let uk = "Update Key"
    let v = 3.5 :: Float
    let ck = "Check Key"

    it "returns 0.0 on updated key when isTerminal=True" $ do
      let decay = 0.0 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, 1.0)
      tabularValue (const True) defv ut uk `shouldBe` 0.0

    it "returns 0.0 on check key when isTerminal=True" $ do
      let decay = 0.0 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, 1.0)
      tabularValue (const True) defv ut ck `shouldBe` 0.0

    it "returns new value on updated key when isTerminal=False" $ do
      let decay = 0.0 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, 1.0)
      tabularValue (const False) defv ut uk `shouldBe` 3.5

    it "returns defv value on check key when isTerminal=False" $ do
      let decay = 0.0 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, 1.0)
      tabularValue (const False) defv ut ck  `shouldBe` 2.0

    it "returns partial update for decay=0.8" $ do
      let decay = 0.8 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, 1.0)
      tabularValue (const False) defv ut uk  `shouldBe` 3.5

    it "returns overwrite for decay=1.0" $ do
      let decay = 1.0 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, 1.0)
      tabularValue (const False) defv ut uk `shouldBe` 3.5

    it "returns defvar on update key when isTerminal=False" $ do
      let decay = 0.0 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, 1.0)
      tabularVar (const False) infinity ut ck `shouldBe` infinity

    it "returns defvar on check key when isTerminal=False" $ do
      let decay = 0.0 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, 1.0)
      tabularVar (const False) infinity ut ck `shouldBe` infinity


  describe "Update into empty list with default weight" $ do
    let defv = 2.0 :: Float
    let defw = 1.0 :: Float
    let t = newValueTable
    let uk = "Update Key"
    let v = 3.5 :: Float

    it "returns correct average: decay=0.0, weight=1.0" $ do
      let decay = 0.0 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, 1.0)
      tabularValue (const False) defv ut uk  `shouldBe` 2.75

    it "returns correct average: decay=0.8, weight=1.0" $ do
      let decay = 0.8 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, 1.0)
      tabularValue (const False) defv ut uk  `shouldBe` 3.25

    it "returns correct average: decay=1.0, weight=1.0" $ do
      let decay = 1.0 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, 1.0)
      tabularValue (const False) defv ut uk  `shouldBe` 3.5

    it "returns correct var: decay=0.0, weight=1.0" $ do
      let decay = 0.0 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, 1.0)
      tabularVar (const False) infinity ut uk  `shouldBe` 0.5625

    it "returns correct var: decay=0.8, weight=1.0" $ do
      let decay = 0.8 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, 1.0)
      tabularVar (const False) infinity ut uk  `shouldBe` 0.3125

    it "returns correct var: decay=1.0, weight=1.0" $ do
      let decay = 1.0 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, 1.0)
      tabularVar (const False) infinity ut uk  `shouldBe` 0.0

  describe "Update into empty list with default weight and obs weight" $ do
    let defv = 2.0 :: Float
    let defw = 1.0 :: Float
    let t = newValueTable
    let uk = "Update Key"
    let v = 3.5 :: Float
    let w = 4.0

    it "returns correct average: decay=0.0, weight=4.0" $ do
      let decay = 0.0 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, w)
      tabularValue (const False) defv ut uk  `shouldBe` 3.2

    it "returns correct average: decay=0.8, weight=4.0" $ do
      let decay = 0.8 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, w)
      tabularValue (const False) defv ut uk  `shouldBe` 3.428571429

    it "returns correct average: decay=1.0, weight=4.0" $ do
      let decay = 1.0 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, w)
      tabularValue (const False) defv ut uk  `shouldBe` 3.5

    it "returns correct var: decay=0.0, weight=4.0" $ do
      let decay = 0.0 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, 4.0)
      tabularVar (const False) infinity ut uk  `shouldBe` 0.36

    it "returns correct var: decay=0.8, weight=4.0" $ do
      let decay = 0.8 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, 4.0)
      tabularVar (const False) infinity ut uk  `shouldBe` 0.1020408163

    it "returns correct var: decay=1.0, weight=4.0" $ do
      let decay = 1.0 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, 4.0)
      tabularVar (const False) infinity ut uk  `shouldBe` 0.0

  describe "Multiple simple updates" $ do
    let defv = 2.0 :: Float
    let defw = 0.0 :: Float
    let t = newValueTable
    let d = [(1 :: Int, 1.8 :: Float), (1, 3.5), (2, 2.4), (1, 2.6), (2, 1.3)]

    it "averages across observations (key=1)" $ do
      let decay = 0.0 :: Float
      let ut = foldl (\a (k, v) -> updateValueTable defv defw decay a (k, v, 1.0)) t d
      tabularValue (const False) defv ut 1  `shouldBe` 2.63333333333

    it "averages across observations (key=2)" $ do
      let decay = 0.0 :: Float
      let ut = foldl (\a (k, v) -> updateValueTable defv defw decay a (k, v, 1.0)) t d
      tabularValue (const False) defv ut 2  `shouldBe` 1.85

    it "vars across observations (key=1)" $ do
      let decay = 0.0 :: Float
      let ut = foldl (\a (k, v) -> updateValueTable defv defw decay a (k, v, 1.0)) t d
      tabularVar (const False) infinity ut 1  `shouldBe` 0.3343209877

    it "vars across observations (key=2)" $ do
      let decay = 0.0 :: Float
      let ut = foldl (\a (k, v) -> updateValueTable defv defw decay a (k, v, 1.0)) t d
      tabularVar (const False) infinity ut 2  `shouldBe` 0.1936000000

  describe "Multiple weighted updates with default weight" $ do
    let defv = 2.0 :: Float
    let defw = 1.0 :: Float
    let t = newValueTable
    let d = [(1 :: Int, 1.8 :: Float, 2.0 :: Float), (1, 3.5, 1.0), (2, 2.4, 3.0), (1, 2.6, 1.6), (2, 1.3, 1.5)]

    it "averages across observations (key=1, decay=0.0)" $ do
      let decay = 0.0 :: Float
      let ut = foldl (\a (k, v, w) -> updateValueTable defv defw decay a (k, v, w)) t d
      tabularValue (const False) defv ut 1  `shouldBe` 2.36785714

    it "averages across observations (key=1, decay=0.8)" $ do
      let decay = 0.8 :: Float
      let ut = foldl (\a (k, v, w) -> updateValueTable defv defw decay a (k, v, w)) t d
      tabularValue (const False) defv ut 1  `shouldBe` 2.65889831

    it "averages across observations (key=1, decay=1.0)" $ do
      let decay = 1.0 :: Float
      let ut = foldl (\a (k, v, w) -> updateValueTable defv defw decay a (k, v, w)) t d
      tabularValue (const False) defv ut 1 `shouldBe` 2.6

    it "vars across observations (key=1, decay=0.0)" $ do
      let decay = 0.0 :: Float
      let ut = foldl (\a (k, v, w) -> updateValueTable defv defw decay a (k, v, w)) t d
      tabularVar (const False) infinity ut 1 `shouldBe` 2.36785714

    it "vars across observations (key=1, decay=0.8)" $ do
      let decay = 0.8 :: Float
      let ut = foldl (\a (k, v, w) -> updateValueTable defv defw decay a (k, v, w)) t d
      tabularVar (const False) infinity ut 1 `shouldBe` 2.65889831

    it "vars across observations (key=1, decay=1.0)" $ do
      let decay = 1.0 :: Float
      let ut = foldl (\a (k, v, w) -> updateValueTable defv defw decay a (k, v, w)) t d
      tabularVar (const False) infinity ut 1 `shouldBe` 2.6

    it "averages across observations (key=2, decay=0.0)" $ do
      let decay = 0.0 :: Float
      let ut = foldl (\a (k, v, w) -> updateValueTable defv defw decay a (k, v, w)) t d
      tabularValue (const False) defv ut 2 `shouldBe` 2.027273

    it "averages across observations (key=2, decay=0.8)" $ do
      let decay = 0.8 :: Float
      let ut = foldl (\a (k, v, w) -> updateValueTable defv defw decay a (k, v, w)) t d
      tabularValue (const False) defv ut 2 `shouldBe` 1.62149532

    it "averages across observations (key=2, decay=1.0)" $ do
      let decay = 1.0 :: Float
      let ut = foldl (\a (k, v, w) -> updateValueTable defv defw decay a (k, v, w)) t d
      tabularValue (const False) defv ut 2 `shouldBe` 1.3

    it "vars across observations (key=2, decay=0.0)" $ do
      let decay = 0.0 :: Float
      let ut = foldl (\a (k, v, w) -> updateValueTable defv defw decay a (k, v, w)) t d
      tabularVar (const False) infinity ut 2 `shouldBe` 2.02727272

    it "vars across observations (key=2, decay=0.8)" $ do
      let decay = 0.8 :: Float
      let ut = foldl (\a (k, v, w) -> updateValueTable defv defw decay a (k, v, w)) t d
      tabularVar (const False) infinity ut 2 `shouldBe` 1.62149532

    it "vars across observations (key=2, decay=1.0)" $ do
      let decay = 1.0 :: Float
      let ut = foldl (\a (k, v, w) -> updateValueTable defv defw decay a (k, v, w)) t d
      tabularVar (const False) infinity ut 2 `shouldBe` 1.3


  describe "Update with 0 weight" $ do
    let defv = 2.0 :: Float
    let defw = 1.0 :: Float
    let t = newValueTable
    let uk = "Update Key"
    let v = 3.5 :: Float
    let w = 0.0

    it "returns the defv even after update: decay=0.0, weight=0.0" $ do
      let decay = 0.0 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, w)
      fEq (tabularValue (const False) defv ut uk) 2.0 `shouldBe` True

    it "returns the defv even after update: decay=0.0, weight=0.4" $ do
      let decay = 0.4 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, w)
      fEq (tabularValue (const False) defv ut uk) 2.0 `shouldBe` True

    it "returns the defv even after update: decay=0.0, weight=0.8" $ do
      let decay = 0.8 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, w)
      fEq (tabularValue (const False) defv ut uk) 2.0 `shouldBe` True

    it "returns the defvar even after update: decay=0.0, weight=0.0" $ do
      let decay = 0.0 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, w)
      fEq (tabularVar (const False) infinity ut uk) infinity `shouldBe` True

    it "returns the defvar even after update: decay=0.0, weight=0.4" $ do
      let decay = 0.4 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, w)
      fEq (tabularVar (const False) infinity ut uk) infinity `shouldBe` True

    it "returns the defvar even after update: decay=0.0, weight=0.8" $ do
      let decay = 0.8 :: Float
      let ut = updateValueTable defv defw decay t (uk, v, w)
      fEq (tabularVar (const False) infinity ut uk) infinity `shouldBe` True


