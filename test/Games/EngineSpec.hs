module Games.EngineSpec ( spec ) where

import Test.Hspec ( shouldBe, it, describe, Spec )
import Games.Engine as Engine ( episodeReturns )


spec :: Spec
spec = do
  describe "Empty steps" $ do
    let steps = []
    it "returns empty list when steps empty (discount=1.0)" $ do
      let r = episodeReturns (1.0 :: Float) steps
      null r `shouldBe` True

    it "returns empty list when steps empty (discount=0.95)" $ do
      let r = episodeReturns (1.0 :: Float) steps
      null r `shouldBe` True

  describe "Single step" $ do
    let steps = [("state", "move", [("a", 10.0::Float), ("b", 20.0)], "next state")]
    let expected = [("state", "move", [("a", 10.0), ("b", 20.0)])]
    it "returns state (discount=1.0)" $ do
      let r = episodeReturns 1.0 steps
      r == expected `shouldBe` True

    it "returns state (discount=0.95)" $ do
      let r = episodeReturns 0.95 steps
      r == expected `shouldBe` True

  describe "Multiple steps" $ do
    let steps = [("state", "move", [("a", 8.0::Double), ("b", 16.0)], "next state"),
                 ("state", "move", [("a", 9.0::Double), ("b", 18.0)], "next state"),
                 ("state", "move", [("a", 10.0::Double), ("b", 20.0)], "next state")]
    it "returns sum of states (discount=1.0)" $ do
      let r = episodeReturns 1.0 steps
      let expected = [("state", "move", [("a", 27.0), ("b", 54.0)]),
                      ("state", "move", [("a", 19.0), ("b", 38.0)]),
                      ("state", "move", [("a", 10.0), ("b", 20.0)])]
      r == expected `shouldBe` True

    it "returns discounted sum of states (discount=0.95)" $ do
      let r = episodeReturns 0.95 steps
      let expected = [("state", "move", [("a", 25.575), ("b", 51.150)]),
                      ("state", "move", [("a", 18.5), ("b", 37.0)]),
                      ("state", "move", [("a", 10.0), ("b", 20.0)])]
      r == expected `shouldBe` True
