module Games.Rules.NimSpec ( spec ) where

import Test.Hspec ( shouldBe, it, describe, Spec )
import Games.Rules.Nim ( newGame )

spec :: Spec
spec = do
  describe "2-player small game to test interface (isMisere=False)" $ do
    let p = ["Bob", "Alice"]
    let initialPiles = [4, 0, 0]
    let isMisere = False
    let (initialState, players, isTerminal, currentPlayer, moves, reward, nextState) = newGame p initialPiles isMisere

    it "generates the right list of players" $ do
      players `shouldBe` p

    it "generates a non-terminal initial state" $ do
      isTerminal initialState `shouldBe` False

    it "reports the correct player for initial state" $ do
      currentPlayer initialState `shouldBe` "Bob"

    it "reports the correct moves for initial state" $ do
      moves initialState `shouldBe` [[1, 0, 0], [2, 0, 0], [3, 0, 0], [4, 0, 0]]

    it "generates the correct reward - Bob" $ do
      reward initialState [4, 0, 0] "Bob" `shouldBe` (1.0::Double)

    it "generates the correct reward - Alice" $ do
      reward initialState [4, 0, 0] "Bob" `shouldBe` (1.0::Double)

      reward initialState [4, 0, 0] "Alice" `shouldBe` -1.0

    it "generates a terminal next state" $ do
      let ns = nextState initialState [4, 0, 0]
      isTerminal ns `shouldBe` True
      moves ns `shouldBe` []
      currentPlayer ns `shouldBe` "Alice"

  describe "2-player Wikipedia to test multiple moves (isMisere=False)" $ do
      let p = ["Bob", "Alice"]
      let initialPiles = [3, 4, 5]
      let isMisere = False
      let (initialState, _, isTerminal, currentPlayer, moves, reward, nextState) = newGame p initialPiles isMisere
      let m = [[2, 0, 0], [0, 0, 3], [0, 1, 0], [0, 1, 0], [1, 0, 0], [0, 1, 0], [0, 0, 1], [0, 1, 0]]
      let s = foldl nextState initialState m

      it "generates a non-terminal last move state" $ do
        isTerminal s `shouldBe` False

      it "generates the correct current player" $ do
        currentPlayer s `shouldBe` "Bob"
        
      it "generates the correct move list" $ do
        moves s `shouldBe` [[0, 0, 1]]
      
      it "generates the correct reward - Bob" $ do
        reward s [0, 0, 1] "Bob" `shouldBe` (1.0::Double)

      it "generates the correct reward - Alice" $ do
        reward s [0, 0, 1] "Alice" `shouldBe` (-1.0::Double)

  describe "2-player small game to test reward (isMisere=True)" $ do
    let p = ["Bob", "Alice"]
    let initialPiles = [4, 0, 0]
    let isMisere = True
    let (initialState, _, _, _, _, reward, _) = newGame p initialPiles isMisere

    it "generates the correct reward - Bob" $ do
      reward initialState [4, 0, 0] "Bob" `shouldBe` (-1.0::Double)

    it "generates the correct reward - Alice" $ do
      reward initialState [4, 0, 0] "Alice" `shouldBe` (1.0::Double)

  describe "3-player game to test reward (isMisere=False)" $ do
      let p = ["Bob", "Alice", "Charlie"]
      let initialPiles = [3, 0, 0]
      let isMisere = False
      let (initialState, _, _, _, _, reward, _) = newGame p initialPiles isMisere

      it "generates the correct reward - Bob" $ do
        reward initialState [3, 0, 0] "Bob" `shouldBe` (2.0::Double)

      it "generates the correct reward - Alice" $ do
        reward initialState [3, 0, 0] "Alice" `shouldBe` (-1.0::Double)

      it "generates the correct reward - Charlie" $ do
        reward initialState [3, 0, 0] "Charlie" `shouldBe` (-1.0::Double)

  describe "3-player game to test reward (isMisere=True)" $ do
        let p = ["Bob", "Alice", "Charlie"]
        let initialPiles = [3, 0, 0]
        let isMisere = True
        let (initialState, _, _, _, _, reward, _) = newGame p initialPiles isMisere

        it "generates the correct reward" $ do
          reward initialState [3, 0, 0] "Bob" `shouldBe` (-2.0::Double)

        it "generates the correct reward" $ do
          reward initialState [3, 0, 0] "Alice" `shouldBe` (1.0::Double)

        it "generates the correct reward" $ do
          reward initialState [3, 0, 0] "Charlie" `shouldBe` (1.0::Double)
