module Games.Rules.DraftSpec ( spec ) where

import Test.Hspec ( shouldBe, it, describe, Spec )
import Games.Rules.Draft ( newGame )

spec :: Spec
spec = do
  describe "2-player small game to test interface" $ do
    let owners = ["Mike", "Other"]
    let rounds = 1
    let isSnake = True
    let p = [("QB1", "Team 1", ["QB"], [35.0])]
    let positionsToRosterSpots = [("QB", ["QB"])]
    let startingRosterSpots = [("QB", 1)]
    let (initialState, players, isTerminal, currentPlayer, moves, reward, nextState) = newGame owners rounds isSnake p positionsToRosterSpots startingRosterSpots

    it "generates the right list of players" $ do
      players `shouldBe` owners

    it "generates a non-terminal initial state" $ do
      isTerminal initialState `shouldBe` False

    it "reports the correct player for initial state" $ do
      currentPlayer initialState `shouldBe` "Mike"

    it "reports the correct moves for initial state" $ do
      moves initialState `shouldBe` [0]

    it "generates the correct reward - player 0 current owner" $ do
      reward initialState 0 "Mike" `shouldBe` (35.0::Double)

    it "generates the correct reward - player 0 other owner" $ do
      reward initialState 0 "Other" `shouldBe` (0.0::Double)

    it "generates a terminal next state" $ do
      let ns = nextState initialState 0
      isTerminal ns `shouldBe` True

    it "increments current player" $ do
      let ns = nextState initialState 0
      currentPlayer ns `shouldBe` "Other"

  describe "2-player VONA game multiple moves" $ do
      let owners = ["Mike", "Other"]
      let rounds = 4
      let isSnake = True
      let p = [ ("QB1", "A", ["QB"], [35.0])
              , ("RB1", "B", ["RB"], [12.0])
              , ("WR1", "C", ["WR"], [20.0])
              , ("KR1", "D", ["KR"], [22.0])
              , ("QB2", "E", ["QB"], [33.0])
              , ("RB2", "F", ["RB"], [7.0])
              , ("WR2", "G", ["WR"], [5.0])
              , ("KR2", "H", ["KR"], [21.0])
              ]
      let positionsToRosterSpots = [("QB", ["Q"]), ("RB", ["R"]), ("WR", ["W"]), ("KR", ["K"])]
      let startingRosterSpots = [("Q", 1), ("R", 1), ("W", 1), ("K", 1)]
      let (initialState, _, _, currentPlayer, _, reward, nextState) = newGame owners rounds isSnake p positionsToRosterSpots startingRosterSpots
      
      it "snakes the draft" $ do
        let m = [2, 0, 1]
        let ns = foldl nextState initialState m
        currentPlayer ns `shouldBe` "Mike"

      it "second player in a position nets no value" $ do
        let m = [2, 0, 1]
        let ns = foldl nextState initialState m
        reward ns 6 "Mike" `shouldBe` (0.0::Double)

  describe "2-player VONA game multiple weeks" $ do
      let owners = ["Mike", "Other"]
      let rounds = 4
      let isSnake = True
      let p = [ ("QB1", "A", ["QB"], [35.0, 35.0])
              , ("RB1", "B", ["RB"], [12.0, 12.0])
              , ("WR1", "C", ["WR"], [20.0, 20.0])
              , ("KR1", "D", ["KR"], [22.0, 22.0])
              , ("QB2", "E", ["QB"], [33.0, 33.0])
              , ("RB2", "F", ["RB"], [7.0, 7.0])
              , ("WR2", "G", ["WR"], [5.0, 5.0])
              , ("KR2", "H", ["KR"], [21.0, 21.0])
              ]
      let positionsToRosterSpots = [("QB", ["Q"]), ("RB", ["R"]), ("WR", ["W"]), ("KR", ["K"])]
      let startingRosterSpots = [("Q", 1), ("R", 1), ("W", 1), ("K", 1)]
      let (initialState, _, _, _, _, reward, _) = newGame owners rounds isSnake p positionsToRosterSpots startingRosterSpots
      
      it "captures multiple weeks - player 0" $ do
        reward initialState 0 "Mike" `shouldBe` (70.0::Double)

      it "captures multiple weeks - player 6" $ do
        reward initialState 6 "Mike" `shouldBe` (10.0::Double)
