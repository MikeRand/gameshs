module Games.Policy.SoftSpec ( spec ) where

import Test.Hspec ( shouldBe, it, describe, Spec )
import Games.Policy.Soft as Soft ( ppf, pmf, finitePmf )
import Games.Policy.GreedyV as GreedyV ( ppf, pmf )


spec :: Spec
spec = do
  let initialState = "Initial state"
  let players = [1::Int, 2, 3]
  let isTerminal = (== "Terminal state")
  let currentPlayer = const (1::Int)
  let moves s
        | isTerminal s = []
        | otherwise = ["a", "b", "c", "d", "e"]
  let nextState _ _ = "Terminal state"
  let defeq a b = abs (a - b) < 1e-6

  describe "Single-best choice" $ do

      let reward s m _
            | isTerminal s = 0.00000001::Float
            | m == "a" = 2.0000001
            | otherwise = 1.00000001
      let game = (initialState, players, isTerminal, currentPlayer, moves, reward, nextState)
      let stateValue (_, _) = 10.0

      describe "ppf" $ do
        let gvPpf = GreedyV.ppf game defeq stateValue
        let sPpf = Soft.ppf game 0.3 gvPpf

        it "Median" $ do
            sPpf "Initial state" 0.5 `shouldBe` "a"
        
        it "0.25" $ do
            sPpf "Initial state" 0.25 `shouldBe` "e"

        it "0.75" $ do
            sPpf "Initial state" 0.75 `shouldBe` "a"

      describe "pmf" $ do
        let gvPmf = GreedyV.pmf game defeq stateValue
        let sPmf = Soft.pmf game 0.3 gvPmf

        it "Max choices should be 76%" $ do
            all (\m-> defeq (sPmf "Initial state" m) 0.76) ["a"] `shouldBe` True
        
        it "All others should be 6%" $ do
            all (\m-> defeq (sPmf "Initial state" m) 0.06) ["b", "c", "d", "e"] `shouldBe` True

      describe "finitePmf" $ do
        let gvPmf = GreedyV.pmf game defeq stateValue
        let sFinitePmf = Soft.finitePmf game 0.3 gvPmf

        it "All choices" $ do
            sFinitePmf "Initial state" `shouldBe` [("a", 0.76), ("b", 6.0000002e-2), ("c", 6.0000002e-2), ("d", 6.0000002e-2), ("e", 6.0000002e-2)]


  describe "4 choices tied for best" $ do
    let reward s m _
          | isTerminal s = 0.00000001::Float
          | m /= "a" = 2.0000001
          | otherwise = 1.00000001
    let game = (initialState, players, isTerminal, currentPlayer, moves, reward, nextState)
    let stateValue (_, _) = 10.0

    describe "ppf" $ do
      let gvPpf = GreedyV.ppf game defeq stateValue
      let sPpf = Soft.ppf game 0.3 gvPpf

      it "Median" $ do
          sPpf "Initial state" 0.5 `shouldBe` "c"
      
      it "0.1" $ do
          sPpf "Initial state" 0.1 `shouldBe` "b"

      it "0.4" $ do
          sPpf "Initial state" 0.4 `shouldBe` "b"

      it "0.6" $ do
          sPpf "Initial state" 0.6 `shouldBe` "c"

      it "0.9" $ do
          sPpf "Initial state" 0.9 `shouldBe` "e"

      describe "pmf" $ do
        let gvPmf = GreedyV.pmf game defeq stateValue
        let sPmf = Soft.pmf game 0.3 gvPmf
        it "Max choices should be 23.5%" $ do
            all (\m-> defeq (sPmf "Initial state" m) 0.235) ["b", "c", "d", "e"] `shouldBe` True
        
        it "All others should be 6%" $ do
            all (\m-> defeq (sPmf "Initial state" m) 0.06) ["a"] `shouldBe` True

      describe "finitePmf" $ do
        let gvPmf = GreedyV.pmf game defeq stateValue
        let sFinitePmf = Soft.finitePmf game 0.3 gvPmf
        it "Max choices only" $ do
            sFinitePmf "Initial state" `shouldBe` [("a", 6.0000002e-2), ("b", 0.235), ("c", 0.235), ("d", 0.235), ("e", 0.235)]


  describe "All 5 choices tied for best" $ do
    let reward s _ _
          | isTerminal s = 0.00000001::Float
          | otherwise = 1.00000001
    let game = (initialState, players, isTerminal, currentPlayer, moves, reward, nextState)
    let stateValue (_, _) = 10.0

    describe "ppf" $ do
      let gvPpf = GreedyV.ppf game defeq stateValue
      let sPpf = Soft.ppf game 0.3 gvPpf

      it "Median" $ do
          sPpf "Initial state" 0.5 `shouldBe` "b"
      
      it "0.1" $ do
          sPpf "Initial state" 0.1 `shouldBe` "b"

      it "0.3" $ do
          sPpf "Initial state" 0.3 `shouldBe` "a"

      it "0.7" $ do
          sPpf "Initial state" 0.7 `shouldBe` "c"

      it "0.9" $ do
          sPpf "Initial state" 0.9 `shouldBe` "e"

    describe "pmf" $ do
      let gvPmf = GreedyV.pmf game defeq stateValue
      let sPmf = Soft.pmf game 0.3 gvPmf
      it "Max choices should be 20%" $ do
        all (\m-> defeq (sPmf "Initial state" m) 0.2) ["a", "b", "c", "d", "e"] `shouldBe` True

    describe "finitePmf" $ do
        let gvPmf = GreedyV.pmf game defeq stateValue
        let sFinitePmf = Soft.finitePmf game 0.3 gvPmf
        it "Max choices only" $ do
            sFinitePmf "Initial state" `shouldBe` [("a", 0.2), ("b", 0.2), ("c", 0.2), ("d", 0.2), ("e", 0.2)]


  describe "All 0" $ do
    let reward _ _ _ = 0
    let game = (initialState, players, isTerminal, currentPlayer, moves, reward, nextState)
    let stateValue (_, _) = 10.0

    describe "ppf" $ do
      let gvPpf = GreedyV.ppf game defeq stateValue
      let sPpf = Soft.ppf game 0.3 gvPpf

      it "Median" $ do
          sPpf "Initial state" (0.5::Float) `shouldBe` "b"
      
      it "0.1" $ do
          sPpf "Initial state" 0.1 `shouldBe` "b"

      it "0.3" $ do
          sPpf "Initial state" 0.3 `shouldBe` "a"

      it "0.7" $ do
          sPpf "Initial state" 0.7 `shouldBe` "c"

      it "0.9" $ do
          sPpf "Initial state" 0.9 `shouldBe` "e"

    describe "pmf" $ do
        let gvPmf = GreedyV.pmf game defeq stateValue
        let sPmf = Soft.pmf game 0.3 gvPmf
        it "Max choices should be 20%" $ do
            all (\m-> defeq (sPmf "Initial state" m) 0.2) ["a", "b", "c", "d", "e"] `shouldBe` True

    describe "finitePmf" $ do
        let gvPmf = GreedyV.pmf game defeq stateValue
        let sFinitePmf = Soft.finitePmf game 0.3 gvPmf
        it "Max choices only" $ do
            sFinitePmf "Initial state" `shouldBe` [("a", 0.2), ("b", 0.2), ("c", 0.2), ("d", 0.2), ("e", 0.2)]
