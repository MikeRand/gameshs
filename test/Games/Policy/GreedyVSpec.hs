module Games.Policy.GreedyVSpec ( spec ) where

import Test.Hspec ( shouldBe, it, describe, Spec )
import Games.Policy.GreedyV as GreedyV ( ppf, pmf, finitePmf )


spec :: Spec
spec = do
  let initialState = "Initial state"
  let players = [1::Int, 2, 3]
  let isTerminal = (== "Terminal state")
  let currentPlayer = const (1::Int)
  let moves s
        | isTerminal s = []
        | otherwise = ["a", "b", "c", "d", "e"]
  let nextState _ _ = "Terminal state"
  let defeq a b = abs (a - b) < 1e-6

  describe "Single-best choice" $ do

      let reward s m _
            | isTerminal s = 0.00000001::Float
            | m == "a" = 2.0000001
            | otherwise = 1.00000001
      let game = (initialState, players, isTerminal, currentPlayer, moves, reward, nextState)
      let stateValue (_, _) = 10.0

      describe "ppf" $ do
        let gvPpf = ppf game defeq stateValue
        it "Median should be max choice" $ do
            gvPpf "Initial state" 0.5 `shouldBe` "a"
        
        it "0.25 should be max choice" $ do
            gvPpf "Initial state" 0.25 `shouldBe` "a"

        it "0.75 should be max choice" $ do
            gvPpf "Initial state" 0.75 `shouldBe` "a"

      describe "pmf" $ do
        let gvPmf = pmf game defeq stateValue
        it "Max choices should be 100%" $ do
            all (\m-> defeq (gvPmf "Initial state" m) 1.0) ["a"] `shouldBe` True
        
        it "All others should be 0" $ do
            all (\m-> defeq (gvPmf "Initial state" m) 0.0) ["b", "c", "d", "e"] `shouldBe` True

      describe "finitePmf" $ do
        let gvFinitePmf = finitePmf game defeq stateValue
        it "Max choices only" $ do
            gvFinitePmf "Initial state" `shouldBe` [("a", 1.0)]


  describe "4 choices tied for best" $ do
    let reward s m _
          | isTerminal s = 0.00000001::Float
          | m /= "a" = 2.0000001
          | otherwise = 1.00000001
    let game = (initialState, players, isTerminal, currentPlayer, moves, reward, nextState)
    let stateValue (_, _) = 10.0

    describe "ppf" $ do
      let gvPpf = ppf game defeq stateValue

      it "Median should 3rd choice" $ do
          gvPpf "Initial state" 0.5 `shouldBe` "d"
      
      it "0.1 should be 1st choice" $ do
          gvPpf "Initial state" 0.1 `shouldBe` "b"

      it "0.4 should be 2nd choice" $ do
          gvPpf "Initial state" 0.4 `shouldBe` "c"

      it "0.6 should be 3rd choice" $ do
          gvPpf "Initial state" 0.6 `shouldBe` "d"

      it "0.9 should be 4th choice" $ do
          gvPpf "Initial state" 0.9 `shouldBe` "e"

      describe "pmf" $ do
        let gvPmf = pmf game defeq stateValue
        it "Max choices should be 25%" $ do
            all (\m-> defeq (gvPmf "Initial state" m) 0.25) ["b", "c", "d", "e"] `shouldBe` True
        
        it "All others should be 0" $ do
            all (\m-> defeq (gvPmf "Initial state" m) 0.0) ["a"] `shouldBe` True

      describe "finitePmf" $ do
        let gvFinitePmf = finitePmf game defeq stateValue
        it "Max choices only" $ do
            gvFinitePmf "Initial state" `shouldBe` [("b", 0.25), ("c", 0.25), ("d", 0.25), ("e", 0.25)]

  describe "All 5 choices tied for best" $ do
    let reward s _ _
          | isTerminal s = 0.00000001::Float
          | otherwise = 1.00000001
    let game = (initialState, players, isTerminal, currentPlayer, moves, reward, nextState)
    let stateValue (_, _) = 10.0

    describe "ppf" $ do
      let gvPpf = ppf game defeq stateValue

      it "Median should 3rd choice" $ do
          gvPpf "Initial state" 0.5 `shouldBe` "c"
      
      it "0.1 should be 1st choice" $ do
          gvPpf "Initial state" 0.1 `shouldBe` "a"

      it "0.3 should be 2nd choice" $ do
          gvPpf "Initial state" 0.3 `shouldBe` "b"

      it "0.7 should be 4th choice" $ do
          gvPpf "Initial state" 0.7 `shouldBe` "d"

      it "0.9 should be 5th choice" $ do
          gvPpf "Initial state" 0.9 `shouldBe` "e"

    describe "pmf" $ do
      let gvPmf = pmf game defeq stateValue
      it "Max choices should be 20%" $ do
        all (\m-> defeq (gvPmf "Initial state" m) 0.2) ["a", "b", "c", "d", "e"] `shouldBe` True

    describe "finitePmf" $ do
        let gvFinitePmf = finitePmf game defeq stateValue
        it "Max choices only" $ do
            gvFinitePmf "Initial state" `shouldBe` [("a", 0.2), ("b", 0.2), ("c", 0.2), ("d", 0.2), ("e", 0.2)]



  describe "All 0" $ do
    let reward _ _ _ = 0
    let game = (initialState, players, isTerminal, currentPlayer, moves, reward, nextState)
    let stateValue (_, _) = 10.0

    describe "ppf" $ do
      let gvPpf = ppf game defeq stateValue

      it "Median should 3rd choice" $ do
          gvPpf "Initial state" (0.5::Float) `shouldBe` "c"
      
      it "0.1 should be 1st choice" $ do
          gvPpf "Initial state" 0.1 `shouldBe` "a"

      it "0.3 should be 2nd choice" $ do
          gvPpf "Initial state" 0.3 `shouldBe` "b"

      it "0.7 should be 4th choice" $ do
          gvPpf "Initial state" 0.7 `shouldBe` "d"

      it "0.9 should be 5th choice" $ do
          gvPpf "Initial state" 0.9 `shouldBe` "e"

    describe "pmf" $ do
        let gvPmf = pmf game defeq stateValue
        it "Max choices should be 20%" $ do
            all (\m-> defeq (gvPmf "Initial state" m) 0.2) ["a", "b", "c", "d", "e"] `shouldBe` True

    describe "finitePmf" $ do
        let gvFinitePmf = finitePmf game defeq stateValue
        it "Max choices only" $ do
            gvFinitePmf "Initial state" `shouldBe` [("a", 0.2), ("b", 0.2), ("c", 0.2), ("d", 0.2), ("e", 0.2)]

